# GitOps Fundamentals

---

# About the course

GitOps is a set of best practices that adds to Git's capability of application code management, also feature of `source of truth`. With GitOps you describe your whole infrastructure and applications in a declarative format and use Git for storage, history, and auditing of your deployments.
In this course we'll learn basics of GitOps practice, ArgoCD that can aid in GitOps practice and strategies of deploying with ArgoCD.

---

# The Goal of the course

Main goal is to become fluent in understanding what GitOps is, and how to use ArgoCD in use cases of deployment.

---

# Who is the course for ?

- DevOps juniors
- DevOps Senior
- Data Engineers
- Data Scientists

---

# What will we learn in course? 

- GitOps
    - What is gitops ?
    - GitOps use cases
- ArgoCD
    - Introduction to ArgoCD
    - Installation of ArgoCD
    - Creating application for ArgoCD
    - Syncing application
- Using ArgoCD
    - Application Health
    - Sync Strategies
    - Secret Management
    - Declarative setup
    - Deploying with Helm
    - Deploying with Kustomize
- Progressive Delivery
    - What is Progressive Delivery
    - Introduction to Argo Rollouts 
    - Blue/Green with Argo Rollouts
    - Canaries with Argo Rollouts
    - Automated rollbacks with metrics
    - Using Argo Rollouts with Argo CD

- Summary




